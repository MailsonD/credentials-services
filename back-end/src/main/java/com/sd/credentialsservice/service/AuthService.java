package com.sd.credentialsservice.service;

import com.sd.credentialsservice.config.ApplicationConfig;
import com.sd.credentialsservice.controllers.DTO.AuthDTO;
import com.sd.credentialsservice.exceptions.AuthException;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Base64;

@Service
@AllArgsConstructor
public class AuthService {

    private final ApplicationConfig applicationConfig;

    public String auth(AuthDTO authDTO) throws AuthException {
        ApplicationConfig.DefaultAuth defaultAuth = applicationConfig.getDefaultAuth();
        if(authDTO.getLogin().equals(defaultAuth.getLogin()) && authDTO.getPassword().equals(defaultAuth.getPassword())){
            return new String(Base64.getEncoder().encode((authDTO.getLogin()+":"+authDTO.getPassword()).getBytes()));
        }
        throw new AuthException("Invalid Login or Password");
    }

}
