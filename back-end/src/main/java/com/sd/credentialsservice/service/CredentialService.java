package com.sd.credentialsservice.service;

import com.sd.credentialsservice.domain.Credential;
import com.sd.credentialsservice.repository.CredentialRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

/**
 * @author raul
 * @project credentials-service
 */
@Service
public class CredentialService {

    private Credential credential;
    private final CredentialRepository credentialRepository;

    public CredentialService(CredentialRepository credentialRepository) {
        this.credentialRepository = credentialRepository;
    }

    public Credential saveCredential(Credential credential) throws Exception {
        Credential credentialSaved = null;
        try {
            credential.setHash(generateHash(credential));
            credentialSaved = this.credentialRepository.save(credential);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return credentialSaved;
    }

    public List<Credential> listCredentials() {
        return this.credentialRepository.findAll();
    }

    public void removeCredential(String id) {
        this.credentialRepository.deleteById(id);
    }

    public Credential updateCredential(Credential credential) {
        return this.credentialRepository.save(credential);
    }

    public Optional<Credential> getByHash(String hash) {
        return credentialRepository.findById(hash);
    }

    private String generateHash(Credential credential){
        String drive = credential.getDbDrive();
        String dbname = credential.getDbName();
        String salt = credential.getSalt();

        String hashToken = drive + dbname + salt;
        hashToken = DigestUtils.md5DigestAsHex(hashToken.getBytes());

        return hashToken;
    }

}
