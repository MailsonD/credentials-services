package com.sd.credentialsservice.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "app")
@Data
public class ApplicationConfig {

    private DefaultAuth defaultAuth = new DefaultAuth();

    @Data
    public class DefaultAuth {
        private String login;
        private String password;
        private String role;
    }


}
