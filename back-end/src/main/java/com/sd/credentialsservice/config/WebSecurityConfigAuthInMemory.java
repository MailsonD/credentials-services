package com.sd.credentialsservice.config;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@AllArgsConstructor
public class WebSecurityConfigAuthInMemory {

    private final ApplicationConfig applicationConfig;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .passwordEncoder(passwordEncoder)
                .withUser(applicationConfig.getDefaultAuth().getLogin())
                .password(passwordEncoder.encode(applicationConfig.getDefaultAuth().getPassword()))
                .authorities("ROLE_"+applicationConfig.getDefaultAuth().getRole());
    }

}
