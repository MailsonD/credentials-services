package com.sd.credentialsservice.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author raul
 * @project credentials-service
 */

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Credential {

    @Id
    private String hash;
    private String dbUser;
    private String dbPassword;
    private String dbHost;
    private String dbName;
    private String dbDrive;
    private String salt;

}
