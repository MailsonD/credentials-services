package com.sd.credentialsservice.repository;

import com.sd.credentialsservice.domain.Credential;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author raul
 * @project credentials-service
 */
public interface CredentialRepository extends JpaRepository<Credential, String> {
}
