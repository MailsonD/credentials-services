package com.sd.credentialsservice.controllers.DTO;

import lombok.*;

@Getter
@Setter
public class DataResponseDTO {
    private String message;
    private Integer error;
    private Boolean success;

    public DataResponseDTO(String message, Integer error, Boolean success) {
        this.message = message;
        this.error = error;
        this.success = success;
    }
}
