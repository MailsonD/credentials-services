package com.sd.credentialsservice.controllers.DTO;

import com.sd.credentialsservice.domain.Credential;
import lombok.*;

@Getter
@Setter
public class CredentialDTO extends DataResponseDTO {
    private ClientCredentialDTO data;

    public CredentialDTO(ClientCredentialDTO data) {
        super("Credencial carregada com sucesso!", 200, true);
        this.data = data;
    }

    public CredentialDTO() {
        super("Credencial revogada ou inexistente!", 401, false);
    }
}
