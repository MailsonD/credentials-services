package com.sd.credentialsservice.controllers;

import com.sd.credentialsservice.controllers.DTO.ClientCredentialDTO;
import com.sd.credentialsservice.controllers.DTO.CredentialDTO;
import com.sd.credentialsservice.domain.Credential;
import com.sd.credentialsservice.service.CredentialService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@AllArgsConstructor
@RequestMapping("/services/v1")
public class AccessCredentialController {

    public final CredentialService credentialService;

    @GetMapping("credentials")
    public ResponseEntity<CredentialDTO> getCredentialByHash(@RequestParam String token) {
        Optional<Credential> result = credentialService.getByHash(token);
        return result.map(credential -> ResponseEntity.ok(
                new CredentialDTO(new ClientCredentialDTO(credential))
        )).orElseGet(() -> ResponseEntity.ok(new CredentialDTO()));
    }

}
