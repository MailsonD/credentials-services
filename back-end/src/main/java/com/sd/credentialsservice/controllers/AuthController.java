package com.sd.credentialsservice.controllers;

import com.sd.credentialsservice.controllers.DTO.AuthDTO;
import com.sd.credentialsservice.controllers.DTO.AuthSuccessDTO;
import com.sd.credentialsservice.exceptions.AuthException;
import com.sd.credentialsservice.service.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/auth")
//@CrossOrigin(origins = "*", maxAge = 3600)
@AllArgsConstructor
public class AuthController {

    private final AuthService authService;

    @PostMapping
    public ResponseEntity<AuthSuccessDTO> auth(@RequestBody AuthDTO authDTO) {
        try {
            String token = this.authService.auth(authDTO);

            return ResponseEntity.ok(new AuthSuccessDTO(token));
        } catch (AuthException e) {
            return ResponseEntity.status(401).build();
        }
    }

}
