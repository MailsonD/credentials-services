package com.sd.credentialsservice.controllers.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AuthSuccessDTO {

    private String token;

}
