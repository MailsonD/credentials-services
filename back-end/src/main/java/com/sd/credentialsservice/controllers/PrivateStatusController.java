package com.sd.credentialsservice.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api")
public class PrivateStatusController {

    @GetMapping("status")
    public String helloWorld() {
        return "Well done. Now you're in!";
    }

}
