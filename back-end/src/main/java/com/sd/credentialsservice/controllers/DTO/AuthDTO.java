package com.sd.credentialsservice.controllers.DTO;

import lombok.Data;

@Data
public class AuthDTO {

    private String login;
    private String password;

}
