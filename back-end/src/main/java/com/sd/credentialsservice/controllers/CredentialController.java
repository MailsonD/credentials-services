package com.sd.credentialsservice.controllers;

import com.sd.credentialsservice.controllers.DTO.CredentialDTO;
import com.sd.credentialsservice.domain.Credential;
import com.sd.credentialsservice.repository.CredentialRepository;
import com.sd.credentialsservice.service.CredentialService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * @author raul
 * @project credentials-service
 */
@RestController
@RequestMapping("/api/credentials")
//@CrossOrigin(origins = "*", maxAge = 3600)
@AllArgsConstructor
public class CredentialController {

    public final CredentialService credentialService;

    @PostMapping
    public ResponseEntity<Credential> saveCredential(@RequestBody Credential credential) {
        try {
            credential = credentialService.saveCredential(credential);
        } catch(Exception e) {
            return ResponseEntity.badRequest().header("error", e.getMessage()).build();
        }
        return ResponseEntity.ok(credential);
    }

    @PutMapping
    public Credential updateCredential(@RequestBody Credential credential) {
        return credentialService.updateCredential(credential);
    }

    @DeleteMapping("/{id}")
    public void deleteCredential(@PathVariable("id") String id) {
        credentialService.removeCredential(id);
    }

    @GetMapping
    public List<Credential> getCredentials() {
        return credentialService.listCredentials();
    }



}
