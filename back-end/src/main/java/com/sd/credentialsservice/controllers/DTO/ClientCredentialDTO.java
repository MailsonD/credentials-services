package com.sd.credentialsservice.controllers.DTO;

import com.sd.credentialsservice.domain.Credential;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class ClientCredentialDTO {

    private String user;
    private String pswd;
    private String host;
    private String dbname;
    private String drive;


    public ClientCredentialDTO(Credential credential) {
        this.user = credential.getDbUser();
        this.pswd = credential.getDbPassword();
        this.host = credential.getDbHost();
        this.dbname = credential.getDbName();
        this.drive = credential.getDbDrive();
    }
}
