package com.sd.credentialsservice.exceptions;

public class AuthException extends Exception {

    public AuthException(String message) {
        super(message);
    }

}
