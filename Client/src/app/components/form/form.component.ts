import { Router } from '@angular/router';
import { CredentialService } from './../../services/credential.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Credential } from 'src/app/model/Credential.model';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  form: FormGroup;
  credential: Credential;

  constructor(
    private fb: FormBuilder,
    private credentialService: CredentialService,
    private router: Router

  ) { }

  ngOnInit(): void {
    this.onCreateForm();
  }

  onSendData(): void {
    this.credential = { ...this.form.value };
    this.credentialService.addCredential(this.credential).subscribe(res => {
      if (res) {
        this.router.navigate(['/list']);
      }
    });
  }

  onCreateForm(): void {
    this.form = this.fb.group({
      hash: [''],
      dbUser: ['',
        Validators.compose([Validators.required, Validators.maxLength(20)])
      ],
      dbPassword: ['',
        Validators.compose([Validators.required, Validators.maxLength(20)])],
      dbHost: ['',
        Validators.compose([Validators.required, Validators.maxLength(20)])],
      dbName: ['',
        Validators.compose([Validators.required, Validators.maxLength(20)])],
      dbDrive: ['',
        Validators.compose([Validators.required, Validators.maxLength(20)])],
      salt: ['',
        Validators.compose([Validators.required, Validators.maxLength(20)])]
    });
  }

  hash: string;
  public dbUser: string;
  public dbPassword: string;
  public dbHost: string;
  public dbName: string;
  public dbDrive: string;
  public salt: string;

}
