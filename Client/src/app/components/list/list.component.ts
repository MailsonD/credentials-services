import { DeleteDialogComponent } from './../delete-dialog/delete-dialog.component';
import { Credential } from './../../model/Credential.model';
import { EMPTY, Observable } from 'rxjs';
import { CredentialService } from './../../services/credential.service';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { switchMap, tap } from 'rxjs/operators';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  public credentials$: Observable<Credential[]>;

  constructor(
    private credentialsService: CredentialService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.loadData();
  }

  openDeleteDialog(credential: Credential): void {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      data: {
        credentialHash: credential.hash,
      },
    });

    dialogRef
      .afterClosed()
      .pipe(
        switchMap((credHash) => {
          if (credHash) {
            return this.credentialsService.revokeCredential(credHash);
          } else {
            return EMPTY;
          }
        }),
        tap(() => {
          this.loadData();
        })
      )
      .subscribe();
  }

  loadData(): void {
    this.credentials$ = this.credentialsService.getCredentials();
  }
}
