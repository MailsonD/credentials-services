import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CredentialService } from 'src/app/services/credential.service';

export interface DialogData {
  credentialHash: string;
}

@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.scss'],
})
export class DeleteDialogComponent implements OnInit {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    public dialogRef: MatDialogRef<DeleteDialogComponent>
  ) {}

  ngOnInit(): void {}

  handleDelete(): void {
    this.dialogRef.close(this.data.credentialHash);
  }
}
