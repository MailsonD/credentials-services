export class Credential {
    public hash: string;
    public dbUser: string;
    public dbPassword: string;
    public dbHost: string;
    public dbName: string;
    public dbDrive: string;
    public salt: string;

}