import { map, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { stringify } from '@angular/compiler/src/util';
import { Observable } from 'rxjs';

export class TokenDTO {
  token: string;
}

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  public static USER_SESSION = 'authenticatedUser';

  constructor(private http: HttpClient) {}

  authenticationService(login: string, password: string): Observable<TokenDTO> {
    return this.http
      .post(`/api/auth`, { login, password })
      .pipe(
        tap((response: TokenDTO) =>
          sessionStorage.setItem(
            AuthenticationService.USER_SESSION,
            stringify(response.token)
          )
        )
      );
  }

  logout(): void {
    sessionStorage.removeItem(AuthenticationService.USER_SESSION);
  }

  isUserLoggedIn(): boolean {
    const user = sessionStorage.getItem(AuthenticationService.USER_SESSION);
    return !!user;
  }
}
