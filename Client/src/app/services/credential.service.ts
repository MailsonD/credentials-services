import { Credential } from './../model/Credential.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CredentialService {
  baseUrl = 'api/credentials';
  headers = {
    Authorization: `Basic ${sessionStorage.getItem('authenticatedUser')}`,
  };

  constructor(private httpClient: HttpClient) {}

  getCredentials(): Observable<Credential[]> {
    return this.httpClient.get<Credential[]>(this.baseUrl, {
      headers: this.headers,
    });
  }

  addCredential(credential: Credential): Observable<Credential> {
    return this.httpClient.post<Credential>(this.baseUrl, credential, {
      headers: this.headers,
    });
  }

  revokeCredential(credentialHash: string): Observable<any> {
    return this.httpClient.delete(`${this.baseUrl}/${credentialHash}`, {
      headers: this.headers,
    });
  }
}
